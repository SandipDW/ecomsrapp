package in.nit.exception;

public class CategoryTypeNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public CategoryTypeNotFoundException() {}
	public CategoryTypeNotFoundException(String message) {
		super(message);
	}
}
