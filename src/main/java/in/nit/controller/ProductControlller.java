package in.nit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import in.nit.entity.Product;
import in.nit.service.IBrandService;
import in.nit.service.ICategoryService;
import in.nit.service.IProductService;

@Controller
@RequestMapping("/product")
public class ProductControlller {

	
	@Autowired
	private IProductService service;
	
	@Autowired
	private ICategoryService categoryService;
		
	
	@Autowired
	private IBrandService brandService;
	
	private void commonUi(Model model) {
		model.addAttribute("categories", categoryService.getCategoryIdAndNames("ACTIVE"));
		model.addAttribute("brands", brandService.getBrandIdAndName());
	}

	
//.show register page
	@GetMapping("/register")
	public String showRegister(Model model) {
		commonUi(model);
		return "ProductRegister";
	}
	
	
	@PostMapping("/save")
	public String saveProduct(
		@ModelAttribute Product product,
		
		Model model
		)
	{
		Long id=service.saveProduct(product);
		String message="Product'"+id+"' created!";
		model.addAttribute("message", message);
		commonUi(model);
		return "ProductRegister";
	}
	
	
	
	@GetMapping("/all")
	public String showAll(Model model) 
	{
		
		List<Product> list = service.getAllProducts();
		model.addAttribute("list", list);
		return "ProductData";
	}

}



