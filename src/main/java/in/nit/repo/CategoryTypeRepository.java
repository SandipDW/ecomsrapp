package in.nit.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.nit.entity.CategoryType;

public interface CategoryTypeRepository extends JpaRepository<CategoryType, Long> {

	@Query("SELECT id,name FROM CategoryType")
	List<Object[]>getCategoryTypeIdAndName();
}
