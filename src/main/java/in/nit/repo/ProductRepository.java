package in.nit.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nit.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
