package in.nit.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.nit.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	
	
	@Query("SELECT id,name FROM Category WHERE status=status")
	List<Object[]>getCategoryIdAndNames(String status);
	
}
