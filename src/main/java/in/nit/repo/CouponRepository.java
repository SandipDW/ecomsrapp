package in.nit.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nit.entity.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Long> {

}
