package in.nit.service.impl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.entity.Category;
import in.nit.repo.CategoryRepository;
import in.nit.service.ICategoryService;
import in.nit.util.AppUtil;


@Service
public class CategoryServiceImpl implements ICategoryService {

	@Autowired
	private CategoryRepository repo;
	@Override
	@Transactional
	public Long saveCategory(Category category) {
		// TODO Auto-generated method stub
		return repo.save(category).getId();
	}

	@Override
	public void updateCategory(Category category) {
		// TODO Auto-generated method stub
		repo.save(category);
	}

	@Override
	public void deleteCategory(Long id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
	}

	@Override
	@Transactional
	public Category getOneCategory(Long id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	@Transactional
	public List<Category> getAllCategorys() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Map<Long, String> getCategoryIdAndNames(String status) {
		List<Object[]> list = repo.getCategoryIdAndNames(status);
		return AppUtil.convertListToMapLong(list);

	}

	@Override
	public List<Category> getCategoryByCategoryType(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long totalCategories() {
		// TODO Auto-generated method stub
		return 0;
	}

}
