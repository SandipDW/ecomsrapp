package in.nit.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.entity.Product;
import in.nit.exception.ProductNotFoundException;
import in.nit.repo.ProductRepository;
import in.nit.service.IProductService;


@Service
public class ProductServiceImpl implements IProductService {

	@Autowired
	private ProductRepository repo;
	
	
	@Override
	public Long saveProduct(Product p) {
		// TODO Auto-generated method stub
		return repo.save(p).getId();
	}

	@Override
	public void updateProduct(Product p) {
		// TODO Auto-generated method stub
		
		if(p.getId()==null|| !repo.existsById(p.getId()))
			throw new ProductNotFoundException("Prodduct not found");
		else
			repo.save(p);

	}

	@Override
	public void deleteProduct(Long id) {
		// TODO Auto-generated method stub
		repo.delete(getOneProduct(id));
	}

	@Override
	public Product getOneProduct(Long id) {
		
		return repo.findById(id)
				.orElseThrow(()->new
ProductNotFoundException("Product Not Found"));
		}
				
	

	@Override
	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
