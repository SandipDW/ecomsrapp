package in.nit.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.entity.CategoryType;
import in.nit.repo.CategoryTypeRepository;
import in.nit.service.ICategoryTypeService;
import in.nit.util.AppUtil;

@Service
public class CategoryTypeServiceImpl implements ICategoryTypeService {

	@Autowired
	private CategoryTypeRepository repo;
	@Override
	public Long saveCategoryType(CategoryType categorytype) {
		// TODO Auto-generated method stub
		return repo.save(categorytype).getId();
	}

	@Override
	public void updateCategoryType(CategoryType categoryType) {
		// TODO Auto-generated method stub
			repo.save(categoryType);
	}

	@Override
	public void deleteCategoryType(Long id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
	}

	@Override
	public CategoryType getOneCategoryType(Long id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public List<CategoryType> getAllCategoryTypes() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Map<Integer, String> getCategoryTypeIdAndName() {
		// TODO Auto-generated method stub
		List<Object[]>list=repo.getCategoryTypeIdAndName();
		return AppUtil.convertListToMap(list); 
	}

}
