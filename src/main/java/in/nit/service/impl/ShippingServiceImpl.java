package in.nit.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.entity.Shipping;
import in.nit.exception.ShippingNotFoundException;
import in.nit.repo.ShippingRepository;
import in.nit.service.IShippingService;

@Service
public class ShippingServiceImpl implements IShippingService {

	
	@Autowired
	private ShippingRepository repo; 
	@Override
	public Long saveShipping(Shipping shipping) {
		// TODO Auto-generated method stub
		return repo.save(shipping).getId();
	}

	@Override
	public void updateShipping(Shipping shipping){
		// TODO Auto-generated method stub
		if(shipping.getId()==null||
				!repo.existsById(shipping.getId()))
			throw new ShippingNotFoundException("Shipping not exist");
		else
			repo.save(shipping);
	}

	@Override
	public void deleteShipping(Long id) {
		// TODO Auto-generated method stub
		repo.delete(getOneShipping(id));
	}

	@Override
	public Shipping getOneShipping(Long id) {
		// TODO Auto-generated method stub
		return repo.findById(id).orElseThrow(
				()->new ShippingNotFoundException("Shipping not exist"));
	}

	@Override
	public List<Shipping> getAllShippings() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
