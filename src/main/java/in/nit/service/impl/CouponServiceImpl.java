package in.nit.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.entity.Coupon;
import in.nit.repo.CouponRepository;
import in.nit.service.ICouponService;

@Service
public class CouponServiceImpl implements ICouponService {
 
	@Autowired
	private CouponRepository repo;
	
	@Override
	public Long saveCoupon(Coupon coupon) {
		
		return repo.save(coupon).getId();
	}

	@Override
	public void updateCoupon(Coupon coupon) {
		// TODO Auto-generated method stub
		repo.save(coupon);
	}

	@Override
	public void deleteCoupon(Long id) {
		// TODO Auto-generated method stub
		repo.deleteById(id);
	}

	@Override
	public Coupon getOneCoupon(Long id) {
		// TODO Auto-generated method stub
		return repo.findById(id).get();
	}

	@Override
	public List<Coupon> getAllCoupons() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

}
